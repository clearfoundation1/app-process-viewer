<?php

$lang['process_viewer_app_description'] = 'Додаток «Перегляд процесів» забезпечує табличне відображення всіх процесів, що виконуються в системі. Перелічено конкретну інформацію про кожен процес, включаючи ідентифікатор процесу, час роботи, використання ЦП та пам’яті.';
$lang['process_viewer_app_name'] = 'Перегляд процесів';
$lang['process_viewer_command'] = 'Команда';
$lang['process_viewer_cpu'] = 'CPU';
$lang['process_viewer_id'] = 'ID';
$lang['process_viewer_memory'] = 'Пам`ять';
$lang['process_viewer_owner'] = 'Власник';
$lang['process_viewer_process_cpu_usage'] = 'Використання процесора';
$lang['process_viewer_processes'] = 'Процеси';
$lang['process_viewer_process_memory_usage'] = 'Використання пам`яті';
$lang['process_viewer_process'] = 'Процеси';
$lang['process_viewer_running'] = 'Виконується';
$lang['process_viewer_size'] = 'Розмір';
